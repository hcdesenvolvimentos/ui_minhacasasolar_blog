$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 1,
        dots: true,
        loop: true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});

	//CARROSSEL DE DESTAQUE
	$("#carrossellLinhadoTempo").owlCarousel({
		items : 10,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});
	var carrossellLinhadoTempo = $("#carrossellLinhadoTempo").data('owlCarousel');
	$('#btnMudarCarrossel').click(function(){ carrossellLinhadoTempo.next(); });

	
	let descricaroAtivo = $( ".carrossellLinhadoTempo .item").attr('data-descricao');
  	$(".textoLinhadoTempo p").text(descricaroAtivo);

	$( ".carrossellLinhadoTempo .item").mouseover(function(e) {
	   	let descrcao = $(this).attr('data-descricao');
	   
	   	$(".textoLinhadoTempo p").text(descrcao);
   		$(".carrossellLinhadoTempo .item").removeClass('ativo');
  	}).mouseout(function() {
    	let descricaroAtivo = $( ".carrossellLinhadoTempo .item").attr('data-descricao');
  		$(".textoLinhadoTempo p").text(descricaroAtivo);
  		$(".carrossellLinhadoTempo .item.primeiro").addClass('ativo');

  	});

  	$( ".topo .pesquisar").mouseover(function(e) {
	   	$(this).addClass('abrirAreaPesquisar');
  	}).mouseout(function() {
		$(this).removeClass('abrirAreaPesquisar');
  	});
		
});